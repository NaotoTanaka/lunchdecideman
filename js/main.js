// プログラム名：LunchDecideMAN(ランチ勝手に決めるマン)
// 概要：ランチを勝手に決めてくれます。ついでに今日の運勢も占います（大吉・中吉・凶）
// 作成日：2017/05/18
// 予定：Googleスプレッドシートから店情報を取得＆みんなで編集
// 作成者：田中直登

var gifArr = ["img/gacha_daikichi.gif", "img/gacha_tyuukichi.gif", "img/gacha_kyou.gif"];

$(function() {

    var navItem = document.createElement('li');
    navItem.className = 'globalHeaderNavItem _showDescription';

    var icon = document.createElement('span');
    icon.className = 'globalHeaderNavItem__button content__icon';
    icon.innerHTML = '🍴';
    icon.id = "myMemo";
    navItem.appendChild(icon);

    var floatWindow = document.createElement('div');
    floatWindow.className = "floatWindow";
    $(floatWindow).css("display", "none");
    $(floatWindow).css("z-index", "1002");

    var lunchWindow = document.createElement("div");
    lunchWindow.id = "_lunchWindow";
    lunchWindow.className = "lunchWindowContainer ";


    var titleHeader = document.createElement("h1");
    titleHeader.className = "contentHl floatContentH1";
    lunchWindow.appendChild(titleHeader);

    var element = document.createElement('span');
    element.className = "autotrim";
    element.innerHTML = "ランチ勝手に決めるマン";
    titleHeader.appendChild(element);

    // ×ボタンを生成
    var cancelBtn = document.createElement('span');
    cancelBtn.className = "cancelBtn";
    cancelBtn.id = "cancelBtn";
    cancelBtn.innerHTML = "✖️";
    titleHeader.appendChild(cancelBtn);

    // GIF動画の領域を生成
    var imgWindow = document.createElement("div");
    imgWindow.className = "imgWindow";
    lunchWindow.appendChild(imgWindow);

    // GIF動画
    var gacha_gif = document.createElement("img");
    gacha_gif.id = "gifImg";
    imgWindow.appendChild(gacha_gif);

    // ボタン設置View
    var btnWindow = document.createElement("div");
    btnWindow.className = "btnWindow";
    lunchWindow.appendChild(btnWindow);

    // 店名等の詳細情報表示画面
    var detailWindow = document.createElement("div");
    detailWindow.className = "detailWindow";
    lunchWindow.appendChild(detailWindow);

    // 詳細情報テーブル生成
    var title = '<tr><th class="arrow_box">店名</div></th><td id="gacha_title"></td></tr>'
    var price = '<tr><th class="arrow_box">値段</th><td id="gacha_price"></td></tr>'
    var time = '<tr><th class="arrow_box">営業時間</th><td id="gacha_time"></td></tr>'
    var description = '<tr><th class="arrow_box">料理</th><td id="gacha_description"></td></tr>'
    var url = '<tr><th class="arrow_box">URL</th><td id="gacha_url"></td></tr>'
    var table = '<table class="company"><tbody>' + title + price + time + description + url + '</tbody></table>';
    $(detailWindow).append(table);

    // がちゃボタン設置
    var gachaBtn = document.createElement('button');
    gachaBtn.type = 'button';
    gachaBtn.className = "btn btn-primary";
    gachaBtn.id = "gachaBtn";
    gachaBtn.innerHTML = "お店を決めていただく";
    btnWindow.appendChild(gachaBtn);

    // GIFスキップチェックボタンを設置
    $(btnWindow).append('<label><input type="checkbox" id="check_skip" style="vertical-align:middle;color:red">GIFをスキップ</label>');
    floatWindow.appendChild(lunchWindow);
    $("#_contactWindow").before(floatWindow);


    // Navバーにランチアイコンを設置
    var icon = '<img src="img/memoIcon.png" alt="">'
    $("#_openTaskWindow").before(navItem);

    //ランチアイコンをクリックしたとき
    $(".content__icon").on('click', function() {
        $(floatWindow).css("display", "block");
        initTableView();

    });
    //×ボタンをクリックした時
    $("#cancelBtn").on('click', function() {
        $(floatWindow).css("display", "none");
    });

    // がちゃボタン押した時
    gachaBtn.onclick = function() {
        // テーブルの初期化
        initTableView();
        // GIF動画を流す
        gacha_gif.src = chrome.extension.getURL(selectGIF());

        // JSONから店舗情報を取得
        $.getJSON(chrome.extension.getURL("gachaJSON.json"), function(data) {
            var len = data.length;

            // 0から”JSONdata”数の範囲の乱数を取得
            var val = Math.floor(Math.random() * len);
            console.log(val);
            var img = document.getElementById("gifImg");
            var wait_time = 0; //GIFの待ち時間
            // ”スキップ”チェックボックスにチェックされているか確認
            if ($("#check_skip").prop("checked")) {
                wait_time = 0;
            } else {
                wait_time = 5000; //GIFが4秒で終わるため
            }
            setTimeout(function() {
                img.src = chrome.extension.getURL(data[val].img);
                var target1 = document.getElementById("gacha_title");
                target1.innerHTML = '<b><font color="orange">' + data[val].title + '</b></font>';
                var target2 = document.getElementById("gacha_price");
                target2.innerHTML = data[val].price;
                var target3 = document.getElementById("gacha_time");
                target3.innerHTML = data[val].time;
                var target4 = document.getElementById("gacha_description");
                target4.innerHTML = data[val].description;
                var target5 = document.getElementById("gacha_url");
                target5.innerHTML = '<a href="' + data[val].url + '"target="_blank">食べログで見る</a>';
            }, wait_time);

        });
    };

});

// ランダムにGIF動画を選択
function selectGIF() {
    var len = gifArr.length;
    // 0からgif動画数の範囲の乱数を取得
    var val = Math.floor(Math.random() * len);
    return gifArr[val]
}

// Viewを初期化
function initTableView() {
    $(".detailWindow").empty();
    // 詳細情報テーブル
    var title = '<tr><th class="arrow_box">店名</div></th><td id="gacha_title"></td></tr>'
    var price = '<tr><th class="arrow_box">値段</th><td id="gacha_price"></td></tr>'
    var time = '<tr><th class="arrow_box">営業時間</th><td id="gacha_time"></td></tr>'
    var description = '<tr><th class="arrow_box">料理</th><td id="gacha_description"></td></tr>'
    var url = '<tr><th class="arrow_box">URL</th><td id="gacha_url"></td></tr>'
    var table = '<table class="company"><tbody>' + title + price + time + description + url + '</tbody></table>';
    $(".detailWindow").append(table);

}
